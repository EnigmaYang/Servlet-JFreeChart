package com.marlabs.jfreechart;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

public class PieChart {
	public static BufferedImage getImage() {

		DefaultPieDataset dataset = new DefaultPieDataset();
		dataset.setValue("IPhone 5s", new Double(20));
		dataset.setValue("SamSung Grand", new Double(20));
		dataset.setValue("MotoG", new Double(40));
		dataset.setValue("Nokia Lumia", new Double(10));

		// JFreeChart chart = ChartFactory.createPieChart(//
		// "Mobile Sales", // chart title
		// dataset, // data
		// true, // include legend
		// true, false);

		JFreeChart chart = ChartFactory.createPieChart("Hello", dataset);
		try {
			ImageIO.write(chart.createBufferedImage(100, 100), "jpg", new File("C:\\asdf.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chart.createBufferedImage(400, 400, BufferedImage.TYPE_3BYTE_BGR, null);
	}
}
