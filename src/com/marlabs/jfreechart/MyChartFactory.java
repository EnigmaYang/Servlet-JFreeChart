package com.marlabs.jfreechart;

import java.awt.image.BufferedImage;

public class MyChartFactory {
	static public BufferedImage getImage(int id) {

		switch (id) {
		case 1:
			return PieChart.getImage();
		case 2:
			return BarChart.getImage();
		case 3:
			return LineChart.getImage();
		}
		return null;
	}
}
